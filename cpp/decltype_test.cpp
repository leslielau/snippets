/*
 * =====================================================================================
 *
 *       Filename:  decltype.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/20/2013 11:48:12 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>

void f();



void f1()
{
	int i = 0;
}

typedef	decltype(&f) F_t;

int main()
{
	int i = 0;
	F_t fff = &f1;
	return 0;
}
