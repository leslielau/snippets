/*
 * =====================================================================================
 *
 *       Filename:  default_template_par.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/01/2013 09:39:52 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <iostream>
template<typename T1, int count>
class CTest;

template<typename T1>
class CTest<T1, 1>
{
	public:
	CTest()
	{
		std::cout<<1<<std::endl;
	}

};

template<typename T1>
class CTest<T1, 2>
{
	public:
	CTest()
	{
		std::cout<<2<<std::endl;
	}
};
struct T1
{
	enum {count=10};
};

struct T2
{
	enum {count=11};
};
int main()
{
  CTest<T1, 1> t1;
  CTest<T2, 2> t2;
}
